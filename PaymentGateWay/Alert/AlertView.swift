//
//  AlertView.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

enum AlertStyle {
    case cancel
    case target
}

class AlertView: NSObject {

    var alert = OKAlertController()
    
    func wrapAlert(title: String?,body: String,img: UIImage?) {
        
        alert = OKAlertController.init(title: title, description: body, image: img, style: .alert)
    }
    
    func wrapAlert(title: String?,Arrtibutedbody: String,img: UIImage?) {
        alert = OKAlertController.init(title: title, Attributeddescription: Arrtibutedbody, image: img, style: .alert)
    }

    func addTextField(title: String?,body: String,img: UIImage?) {
        alert = OKAlertController.init(title: title, description: body, image: img, style: .alert)
    }
    
    func AlerTextReturnString() -> String{
        return alert.textFields[0].text!
    }
    
    func addAction(title: String, style: AlertStyle, action: @escaping () -> Void) {
        let sty : PMAlertActionStyle
        if style == .cancel {
            sty = PMAlertActionStyle.cancel
        } else {
            sty = PMAlertActionStyle.default
        }
        alert.addAction(PMAlertAction.init(title: title, style: sty, action: {
            action()
        }))
    }
    
    func addActionPermanent(title: String, style: AlertStyle, action: @escaping () -> Void) {
        let sty : PMAlertActionStyle
        if style == .cancel {
            sty = PMAlertActionStyle.cancel
        } else {
            sty = PMAlertActionStyle.default
        }
        alert.addActionPermanent(PMAlertAction.init(title: title, style: sty, action: {
            action()
        }))
    }
    
    func showAlert(controller: UIViewController? = nil) {
        if let keyWindow = UIApplication.shared.keyWindow {
            keyWindow.endEditing(true)
            alert.view.frame = keyWindow.bounds
            alert.view.tag = 4446
            keyWindow.addSubview(alert.view)
            keyWindow.makeKeyAndVisible()
        }
    }
}
