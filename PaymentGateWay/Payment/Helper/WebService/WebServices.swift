//
//  WebServices.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/24/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit

 enum RequestType : String {
    case get     = "GET"
    case post    = "POST"
    case delete  = "DELETE"
    case put     = "PUT"
}

 protocol OKApiHandler : class {}

 extension OKApiHandler {
    
    func genericApi(url: URL, data: Data?, httpMethod: RequestType, handle :@escaping (_ result: Data?, _ success: Bool) -> Void) {
        PTLoader.shared.show()
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 120
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = httpMethod.rawValue.uppercased()
        
        request.httpBody   = data

        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if error != nil {
                handle(data, false)
            } else {
                handle(data, true)
            }
            PTLoader.shared.hide()
        }
        dataTask.resume()
    }
    
    func getURL(type: URLType) -> String {
        switch type {
        case .production:
            return "https://www.okdollar.co/"
        case .testing:
            return "http://69.160.4.151:8001/"
        }
    }

}
