//
//  OKPayConstants.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/19/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit
import AdSupport

struct OKPayConstants {
    
    struct helper {
        static let storyName        = "OKPayment"
        static let countryStoryName = "Country"
        static let segueLogin     = "segueLogin"
        static let navigationName = "initialNavigation"
        static let verifyKey      = "isVerifiedFirstTime"
    }
    
    struct global {

        static let defaultSet = UserDefaults.standard
        static let uuid       = ASIdentifierManager.shared().advertisingIdentifier.uuidString // "2F069889-BE65-448F-AE7A-734FAEA5B26B"
        
        static func isInternetConnected(_ delegate: OKPaymentDelegates?) -> Bool {
            let connected = Reachability()?.connection != .none
            
            if connected == false {
                let error = NSError.init(domain: OKPayConstants.messagesKeys.noInternetConnection, code: 1020, userInfo: nil)
                delegate?.didEncounteredErrorWhilePayment(errMsg: error)
            }
            
            return  connected
        }
    }
    
    struct messagesKeys {
        static let wrongKeyWarning = "Hi, Your key is wrong. Please contact the OK$ iOS Development Team"
        static let patternError = "Please draw minimum 4 dots pattern".localized
        static let insertSimKey = "insertSimKey"
        static let preloginHeaderKey = "preLoginHeader"
        static let submitKey = "Submit"
        static let noInternetConnection = "No Internet Connected"
        static let mobiileNumberEmpty = "Please Enter Mobile Number"
        static let smsFailed = "SMS failed"
        static let loginObjectSaveKey = "LoginKeySaved"
        static let loginAccessKey = "keyLoginModel"
    }
    
    struct errorCode {
        static let patternCountError = 35
    }
    
    struct url {
        static let verify   = "RestService.svc/ValidateIosNumber?MobileNumber=%@&otp=%@"
        static let preLogin = "RestService.svc/GetAgentRegOtp"
        static let login    = "RestService.svc/Login"
        static let genericPay = "RestService.svc/GenericPayment"
        static let preLoginUrl = "RestService.svc/GetAgentRegOtp"
    }
        
}


var currentLanguage = "my"
var phNumValidationsApi: [PhNumValidationList] = []
var phNumValidationsFile: [PhNumValidationList] = []
var localBundle: Bundle?
let localizationObj = Localization()
var preLoginDict = Dictionary<String,Any>()
