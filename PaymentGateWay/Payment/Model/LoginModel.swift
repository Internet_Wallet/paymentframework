//
//  LoginModel.swift
//  OKPaymentFramework
//
//  Created by Ashish on 12/4/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit

struct LoginApiRequest: Codable {
    let androidVersion, appId, authCode, authCodeStatus: String?
    let bssid, bluetoothAddress, bluetoothName, cellId: String?
    let connectedNetworkType, dateOfBirth, deviceSoftwareVersion, fatherName: String?
    let hiddenSsid, ipAddress, iosOtp, latitude: String?
    let linkSpeed, longitude, macAddress, mobileNumber: String?
    let msid, name, networkCountryIso, networkId: String?
    let networkOperator, networkOperatorName, networkSignal, networkType: String?
    let osType, password, passwordType, profilePic: String?
    let reRegisterOtp, simCountryIso, simOperator, simOperatorName: String?
    let ssid, simId, voiceMailNo, isNetworkRoaming: String?
    
    enum CodingKeys: String, CodingKey {
        case androidVersion = "AndroidVersion"
        case appId = "AppID"
        case authCode = "AuthCode"
        case authCodeStatus = "AuthCodeStatus"
        case bssid = "BSSID"
        case bluetoothAddress = "BluetoothAddress"
        case bluetoothName = "BluetoothName"
        case cellId = "CellId"
        case connectedNetworkType = "ConnectedNetworkType"
        case dateOfBirth = "DateOfBirth"
        case deviceSoftwareVersion = "DeviceSoftwareVersion"
        case fatherName = "FatherName"
        case hiddenSsid = "HiddenSSID"
        case ipAddress = "IPAddress"
        case iosOtp = "IosOtp"
        case latitude = "Latitude"
        case linkSpeed = "LinkSpeed"
        case longitude = "Longitude"
        case macAddress = "MACAddress"
        case mobileNumber = "MobileNumber"
        case msid = "Msid"
        case name = "Name"
        case networkCountryIso = "NetworkCountryIso"
        case networkId = "NetworkID"
        case networkOperator = "NetworkOperator"
        case networkOperatorName = "NetworkOperatorName"
        case networkSignal = "NetworkSignal"
        case networkType = "NetworkType"
        case osType = "OsType"
        case password = "Password"
        case passwordType = "PasswordType"
        case profilePic = "ProfilePic"
        case reRegisterOtp = "ReRegisterOtp"
        case simCountryIso = "SIMCountryIso"
        case simOperator = "SIMOperator"
        case simOperatorName = "SIMOperatorName"
        case ssid = "SSID"
        case simId = "SimId"
        case voiceMailNo = "VoiceMailNo"
        case isNetworkRoaming
    }
}


//MARK:- Generic payment model
struct GenericPaymentRequestModel: Codable {
    let lExternalReference: GenericPaymentLExternalReference?
    let lGeoLocation: GenericPaymentLGeoLocation?
    let lProximity: GenericPaymentLProximity?
    let lTransactions: GenericPaymentLTransactions?
}

struct GenericPaymentLExternalReference: Codable {
    let direction: Bool?
    let endStation, startStation, vendorId: String?
    
    enum CodingKeys: String, CodingKey {
        case direction = "Direction"
        case endStation = "EndStation"
        case startStation = "StartStation"
        case vendorId = "VendorID"
    }
}

struct GenericPaymentLGeoLocation: Codable {
    let cellId, latitude, longitude: String?
    
    enum CodingKeys: String, CodingKey {
        case cellId = "CellID"
        case latitude = "Latitude"
        case longitude = "Longitude"
    }
}

struct GenericPaymentLProximity: Codable {
    let blueToothUsers: [String]?
    let selectionMode: Bool?
    let wifiUsers: [String]?
    
    enum CodingKeys: String, CodingKey {
        case blueToothUsers = "BlueToothUsers"
        case selectionMode = "SelectionMode"
        case wifiUsers = "WifiUsers"
    }
}

struct GenericPaymentLTransactions: Codable {
    let amount, balance: String?
    let bonusPoint: Int?
    let comments, destination, kickBack, localTransactionType: String?
    let mobileNumber: String?
    let mode: Bool?
    let password: String?
    let resultCode: Int?
    let resultDescription, secureToken, transactionId, transactionTime: String?
    let transactionType: String?
    
    enum CodingKeys: String, CodingKey {
        case amount = "Amount"
        case balance = "Balance"
        case bonusPoint = "BonusPoint"
        case comments = "Comments"
        case destination = "Destination"
        case kickBack = "KickBack"
        case localTransactionType = "LocalTransactionType"
        case mobileNumber = "MobileNumber"
        case mode = "Mode"
        case password = "Password"
        case resultCode = "ResultCode"
        case resultDescription = "ResultDescription"
        case secureToken = "SecureToken"
        case transactionId = "TransactionID"
        case transactionTime = "TransactionTime"
        case transactionType = "TransactionType"
    }
}
