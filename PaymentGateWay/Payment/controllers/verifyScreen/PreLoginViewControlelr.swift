//
//  PreLoginViewControlelr.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/19/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit
import MessageUI
import CoreTelephony

class PreLoginViewControlelr: UIViewController, OKApiHandler, MessageManagerDelegate, HelperFunctions, CountryLeftViewDelegate, PhValidationProtocol {
    
    var kYellowColor = UIColor(red: 255.0 / 255.0, green: 197.0 / 255.0, blue: 2.0 / 255.0, alpha: 1)
   
    var passwordType: String?
    
    
    let validObj = PayToValidations.init()
    private var availableSIM: Bool {
        let device = UIDevice()
        if device.type == .iPhoneXSMax {
            return (CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode == nil && CTTelephonyNetworkInfo().subscriberCellularProvider?.carrierName == nil) ? false : true // sim detection
        } else {
            return (CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode == nil ) ? false : true // sim detection
        }
    }
    
    @IBOutlet weak var clearbutton: UIButton! {
        didSet {
            self.clearbutton.isHidden = true
        }
    }
    
    @IBOutlet weak var languageEnglishView: UIView! {
        didSet {
           // self.languageEnglishView.backgroundColor = UIColor.gray
            self.languageEnglishView.layer.cornerRadius = 15.0
        }
    }
    
    @IBOutlet weak var languageMyanmarView: UIView! {
        didSet {
            //self.languageMyanmarView.backgroundColor = kYellowColor
            self.languageMyanmarView.layer.cornerRadius = 15.0
        }
    }
    
    public var currentSelectedCountry : countrySelected = .myanmarCountry

    enum countrySelected {
        case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
    }
    @IBOutlet weak var mobileNumberField : NoPasteTextField!
    
    @IBOutlet weak var verifyButton      : UIButton!
    
    let alertViewObj = AlertView()
    
    weak var delegate : OKPaymentDelegates?
    
    var model         : OKPayModel?
    
    var countryView   : PaytoViews?
    
    var loginModel    : LoginAccess?
    
    var msgObj        : MessageManager?
    
    var country       : Country?
    
    var isTimerActivated = false
    
    
    
    @IBOutlet weak var headerLabel: UILabel! {
        didSet {
         //   self.headerLabel.text = "Welcome to OK$ Payment Gateway".localized
        }
    }
    @IBOutlet weak var footerLabel: UILabel! {
        didSet {
            self.footerLabel.text = "Copyright @ 2020 Internet Wallet Myanmar Ltd.\n All right reserved".localized
        }
    }
    @IBOutlet weak var versionLabel: UILabel! {
        didSet {
           // self.versionLabel.text = "Version 1.0, Build (1) \n07 March 2020".localized
            self.versionLabel.text = "Version 1.0, Build (1)".localized
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.verifyButton.isUserInteractionEnabled = false
        self.mobileNumberField.layer.borderWidth = 1.0
        self.mobileNumberField.layer.borderColor = UIColor(red: 3.0/255.0, green: 33.0/255.0, blue: 170.0/255.0, alpha: 1.0).cgColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: "Zawgyi-One",size: 18.0)]
        localizationObj.setInitialCurrentLang()
        phNumValidationsFile = getDataFromJSONFile() ?? []
        if OKPayConstants.global.defaultSet.bool(forKey: OKPayConstants.helper.verifyKey) {
            let storyboardBundle = Bundle(for: LoginViewController.self)
            
            let storyboard = UIStoryboard.init(name: OKPayConstants.helper.storyName, bundle: storyboardBundle)
            
            guard let loginController = storyboard.instantiateViewController(withIdentifier: String.init(describing: LoginViewController.self)) as? LoginViewController else { return }
            loginController.model    = self.model
            loginController.delegate = self.delegate
            self.navigationController?.pushViewController(loginController, animated: false)
//            if self.shouldPerformSegue(withIdentifier: OKPayConstants.helper.segueLogin, sender: self) {
//                self.performSegue(withIdentifier: OKPayConstants.helper.segueLogin, sender: self)
//            }
        }

        self.country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
        self.title = "Verify Login"
        countryView = PaytoViews.updateView()
        let countryCode = String(format: "(%@)", "+95")
        countryView?.wrapCountryViewData(img: "myanmar", str: countryCode)
        countryView?.delegate = self
        self.mobileNumberField.leftViewMode = .always
        self.mobileNumberField.leftView     = countryView
        self.hideKeyboardWhenTappedAround()
      
        
        
        if UserDefaults.standard.value(forKey: "currentLanguage") == nil {
            currentLanguage = "my"
            UserDefaults.standard.set("my", forKey: "currentLanguage")
            updateUIMy()
        } else if (UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en" {
            updateUI()
        } else {
            updateUIMy()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    private func updateUI() {
        self.languageEnglishView.backgroundColor = kYellowColor
        self.languageMyanmarView.backgroundColor = UIColor.gray
       self.headerLabel.text = "Welcome to OK$ Payment Gateway"
        self.footerLabel.text = "Copyright @ 2020 Internet Wallet Myanmar Ltd.\n All right reserved"
     //   self.versionLabel.text = "Version 1.0, Build (1) \n01 June 2020"
        verifyButton.setTitle("Submit", for: .normal)
        self.title = "Verify Account"
    }
    
    private func updateUIMy() {
        self.languageEnglishView.backgroundColor = UIColor.gray
        self.languageMyanmarView.backgroundColor = kYellowColor
        self.headerLabel.text = "OK$ ေငြေပးေခ် ဝန္ေဆာင္မႈမွ ႀကိဳဆုိပါသည္။"
        self.footerLabel.text = "Copyright @ 2020 Internet Wallet Myanmar Ltd.\n All right reserved".localized
   //     self.versionLabel.text = "Version 1.0, Build (1) \n01 June 2020".localized
        verifyButton.setTitle("ေငြေပး", for: .normal)
        self.title = "အေကာင့္ထဲ ဝင္ရန္"
    }
       
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissKeyboards))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboards() {
        view.endEditing(true)
    }
    

    @IBAction func enLanguageBtnAction(_ sender: UIButton) {
        currentLanguage = "en"
        languageEnglishView.backgroundColor = kYellowColor
        languageMyanmarView.backgroundColor = UIColor.gray
        UserDefaults.standard.set("en", forKey: "currentLanguage")
        localizationObj.setSeletedlocaLizationLanguage(language: "en")
        updateUI()
    }
    
    @IBAction func myLanguageBtnAction(_ sender: UIButton) {
        currentLanguage = "my"
        languageEnglishView.backgroundColor = UIColor.gray
        languageMyanmarView.backgroundColor = kYellowColor
        UserDefaults.standard.set("my", forKey: "currentLanguage")
        localizationObj.setSeletedlocaLizationLanguage(language: "my")
        updateUIMy()
    }
 
    @IBAction func backButtonAction(_ sender: UIButton) {
    
        if let keyWindow = UIApplication.shared.keyWindow {
            for view in keyWindow.subviews {
                if view.tag == 1473 {
                    view.removeFromSuperview()
                }
            }
        }
        
        self.navigationController?.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func showAlert(_ str: String, image: Any?, simChanges: Bool) {
        // Please enter your valid mobile numbe
        DispatchQueue.main.async {
            
            
            
            self.alertViewObj.wrapAlert(title: "", body: str, img: nil)
            self.alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                 if !simChanges {
                                         self.mobileNumberField.text = (self.country?.dialCode == "+95") ? "09" : ""
                                         self.mobileNumberField.becomeFirstResponder()
                                     }
            })
            self.alertViewObj.showAlert(controller: self)
            
            
            
            
//             let alertView  = UIAlertController(title: "", message: str, preferredStyle: .alert)
//                   alertView.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
//                       if !simChanges {
//                           self.mobileNumberField.text = (self.country?.dialCode == "+95") ? "09" : ""
//                           self.mobileNumberField.becomeFirstResponder()
//                       }
//                   }))
//            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- IBActions
    @IBAction func verifyAction(_ sender: UIButton) {
        
        
        //Made this bool check because problem was coming with timer
     
            if !availableSIM {
                let text = ((UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en") ? "Sim is not available" : "Sim is not available"
                self.showAlert(text, image: nil, simChanges: true)
                return
            }

            let count = self.mobileNumberField.text?.count
            let text = ((UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en") ? "Please enter a valid mobile number" : "ဖုန္းနံပါတ္ မွန္ကန္စြာရိုက္ထည့္ပါ"
            switch currentSelectedCountry {
                
            case .indiaCountry:
                if count! < 10 {
                    showAlert(text, image: nil, simChanges: false)
                    return
                }
            case .myanmarCountry:
                if count! < 9 {
                    self.verifyButton.isUserInteractionEnabled = false
                    showAlert(text, image: nil, simChanges: false)
                    return
                }
            case .chinaCountry:
                if count! < 11 {
                    showAlert(text, image: nil, simChanges: false)
                    return
                }
            case .thaiCountry:
                if count! < 9 {
                    showAlert(text, image: nil, simChanges: false)
                    return
                }
            case .other:
                if (count! < 4) || (count! > 13) {
                    showAlert(text, image:nil, simChanges: false)
                    return
                }
            }

            if OKPayConstants.global.isInternetConnected(self.delegate) {
                self.verifyButton.isUserInteractionEnabled = true
                if !isTimerActivated{
                    self.view.endEditing(true)
                    PTLoader.shared.show()
                     print("******************* pre login API call")
                    self.preLoginApiCalling(agentCode: "")
                }
                
            }else{
                
            }
        
        
    }
    
    //MARK:- Segue to Login
    override  func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let login = segue.destination as? LoginViewController {
            login.model    = self.model
            login.delegate = self.delegate
        }
    }
    
    //MARK:- Message Controller Delegate
     func respondFromMessageManager(result: MessageComposeResult) {
        switch result {
        case .sent:
            self.afterSMSApi()
        case .cancelled:
            //self.delegate?.didEncounteredErrorWhilePayment(errMsg: NSError(domain: OKPayConstants.messagesKeys.smsFailed, code: 31, userInfo: nil))
            break
        case .failed:
            self.showAlert(OKPayConstants.messagesKeys.smsFailed, image: nil, simChanges: false)
            break
        }
    }
    
     func clearButtonShowHide(count: Int) {
        if count > 0 {
            self.clearbutton.isHidden = false
        } else {
            self.clearbutton.isHidden = true
        }
    }
    
    @IBAction func clearButtonAction(_ sender: UIButton) {
        self.clearbutton.isHidden = true
        mobileNumberField.text =  (self.country?.dialCode == "+95") ? "09" : ""
        self.verifyButton.backgroundColor = UIColor.lightGray
        self.mobileNumberField.becomeFirstResponder()
    }

}

//MARK:- Country View Delegate & Functions
extension PreLoginViewControlelr:  CountryViewControllerDelegate {
    
    func countryViewController(_ list: CountryViewController, country: Country) {
        self.country = country
        self.mobileNumberField.leftView = nil
        
        let countryCode = String.init(format: "(%@)", country.dialCode)
        countryView?.wrapCountryViewData(img: country.code, str: countryCode)
        self.mobileNumberField.leftView = countryView
        list.dismiss(animated: true, completion: nil)
        
        switch self.country?.dialCode {
        case "+91":
            currentSelectedCountry = .indiaCountry
        case "+95":
            currentSelectedCountry = .myanmarCountry
        case "+66":
            currentSelectedCountry = .thaiCountry
        case "+86":
            currentSelectedCountry = .chinaCountry
        default:
            currentSelectedCountry  = .other
        }
        
        self.mobileNumberField.text = (self.country?.dialCode == "+95") ? "09" : ""
        self.mobileNumberField.becomeFirstResponder()
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
    
    
    func publicCountryView() {
        guard let countryVC = countryViewController(delegate: self) else { return }
        self.present(countryVC, animated: true, completion: nil)
    }
}

//MARK:- TextField Delegate & Functions
extension PreLoginViewControlelr : UITextFieldDelegate {
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.text =  (self.country?.dialCode == "+95") ? "09" : ""
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if self.country?.dialCode == "+95" {
            textField.text = "09"
        } else {
            textField.text = ""
        }
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        self.verifyButton.isUserInteractionEnabled = true
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        if currentSelectedCountry == .myanmarCountry {
            if range.location <= 1 {
                let newPosition = textField.endOfDocument
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                return false
            }
        }
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
        switch currentSelectedCountry {
        case .indiaCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            clearButtonShowHide(count: textCount)
            if textCount > 10 {
                verifyButton.backgroundColor = kYellowColor
                self.verifyButton.sendActions(for: .touchUpInside)
                return false
            } else {
                verifyButton.backgroundColor = UIColor.lightGray
                return true
            }
        case .chinaCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            clearButtonShowHide(count: textCount)
            if textCount > 11  {
                verifyButton.backgroundColor = kYellowColor
                self.verifyButton.sendActions(for: .touchUpInside)
                return false
            } else {
                verifyButton.backgroundColor = UIColor.lightGray
                return true
            }
        case .myanmarCountry:
            if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
            {
                return false
            }
            
            if range.location == 0 && range.length > 1 {
                textField.text = "09"
                self.clearbutton.isHidden = true
                return false
            }
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            if textCount > 2 {
                self.clearbutton.isHidden = false
            } else {
                self.clearbutton.isHidden = true
            }
            
            let object = validObj.getNumberRangeValidation(text)
           // let validCount = object.min
            
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                if textField.text == "09" {
                    textField.text = "09"
                    self.clearbutton.isHidden = true
                    return false
                }
            }
            
            if object.isRejected == true {
                textField.text = "09"
                self.clearbutton.isHidden = true
                textField.resignFirstResponder()
              //  let text = ((UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en") ? "Rejected Number" : "ျငင္းပယ္မႈ"
                
                self.showAlert("Invalid Mobile Number".localized, image: nil, simChanges: false)
//                if let value = UserDefaults.standard.value(forKey: "currentLanguage") as? String{
//                    if value == "en"{
//                        self.showAlert("Invalid Mobile Number".localized, image: nil, simChanges: false)
//                    }else if  value == "my"{
//                       self.showAlert("ျငင္းပယ္မႈ", image: nil, simChanges: false)
//                    }
//                    
//                }
                
                return false
            }
            
            if textCount < object.min {
                self.verifyButton.isEnabled = false
                self.verifyButton.backgroundColor = .lightGray
            } else if (textCount >= object.min && textCount < object.max) {
                self.verifyButton.isEnabled = true
                self.verifyButton.backgroundColor = kYellowColor
            } else if (textCount == object.max) {
                textField.text = text
                self.verifyButton.backgroundColor = kYellowColor
                self.verifyButton.sendActions(for: .touchUpInside)
                self.verifyButton.isEnabled = true
                return false
            } else if textCount > object.max {
                return false
            }
        case .thaiCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            clearButtonShowHide(count: textCount)
            if textCount > 9  {
                verifyButton.backgroundColor = kYellowColor
                self.verifyButton.sendActions(for: .touchUpInside)
                return false
            }  else {
                verifyButton.backgroundColor = UIColor.lightGray
                return true
            }
        case .other:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            clearButtonShowHide(count: textCount)
            if textCount > 13  {
                verifyButton.backgroundColor = kYellowColor
                self.verifyButton.sendActions(for: .touchUpInside)
                return false
            } else {
                verifyButton.backgroundColor = UIColor.lightGray
                return true
            }
        }
        
        return true
        
    }
    
}

class NoPasteTextField: UITextField {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(paste(_:)) || action == #selector(cut(_:)) || action == #selector(copy(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
           return CGRect(x: 0, y: 6, width: 102 , height: 40)
       }
}
