//
//  PreLoginAfterSMSScenarios.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/27/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit

 extension PreLoginViewControlelr {
    
    
    func afterSMSApi() {
        
        print("****************** is Timer Activated called \(isTimerActivated)")
        if !isTimerActivated{
            PTLoader.shared.show()
            self.isTimerActivated = true
            let storyboardBundle = Bundle(for: PreLoginViewControlelr.self)
            let storyboard = UIStoryboard.init(name: OKPayConstants.helper.storyName, bundle: storyboardBundle)
            guard let loginController = storyboard.instantiateViewController(withIdentifier: String.init(describing: LoginTimerViewController.self)) as? LoginTimerViewController else { return }
            if let keyWindow = UIApplication.shared.keyWindow {
                let screenFrame = UIScreen.main.bounds
                loginController.view.frame = .init(x: screenFrame.width/2 - 64, y: screenFrame.height - 148, width: 128.00, height: 128.00)
                loginController.view.tag = 1473
                keyWindow.addSubview(loginController.view)
                keyWindow.makeKeyAndVisible()
                keyWindow.bringSubviewToFront(loginController.view)
                
                let time : DispatchTime = DispatchTime.now() + 1.0
                DispatchQueue.main.asyncAfter(deadline: time, execute: {
                    self.verifyapiCalling()
                })
            }
        }
    }
    
    
    func verifyapiCalling() {
       
        guard var mobileNumber = self.mobileNumberField.text else {
            return
            
        }
        
        guard var countrycode = self.country?.dialCode else { return }
        
        if countrycode == "+95", mobileNumber.hasPrefix("0") {
            let _ = mobileNumber.remove(at: String.Index.init(encodedOffset: 0))
        }
        
        countrycode  = countrycode.replacingOccurrences(of: "+", with: "00")
        
        mobileNumber = countrycode + mobileNumber

        guard  mobileNumber.count > 0 else {
            self.delegate?.didEncounteredErrorWhilePayment(errMsg: NSError(domain: OKPayConstants.messagesKeys.mobiileNumberEmpty, code: 30, userInfo: nil))
            return
        }
        
        let verifyString = String(format: OKPayConstants.url.verify, mobileNumber, OKPayConstants.global.uuid)
        
        let domain = self.getURL(type: self.model?.urlType ?? .production)
        
        let finalString = domain + verifyString
        
        guard let encodedURLString = finalString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        
        guard let url = URL.init(string: encodedURLString) else { return }
        PTLoader.shared.hide()
        self.genericApi(url: url, data: nil, httpMethod: .get) { (data, success) in
            if success {
                guard let jsonData = data else { return }
                if let otpModel = try? JSONDecoder().decode(OTPVerificationResponse.self, from: jsonData) {
                    print(otpModel)
                    if otpModel.code == 200 {
                        DispatchQueue.main.async {
                             self.isTimerActivated = false
                            let storyborad = UIStoryboard.init(name: "OKPayment", bundle: Bundle.init(for: LoginViewController.self))
                            guard let vc = storyborad.instantiateViewController(withIdentifier: String.init(describing: LoginViewController.self)) as? LoginViewController else {
                                self.log("login pushing segue damaged")
                                return
                            }
                            vc.delegate = self.delegate
                            vc.model = self.model
                            vc.estelResponse = preLoginDict
                            vc.loginModel = self.loginModel
                            vc.agentCode = mobileNumber
                            
                            if let country = self.country {
                                if let passWord = self.passwordType{
                                self.loginModel = LoginAccess(mobile: mobileNumber, country: country, isPattern: passWord)
                                }
                            }
                            
                            self.loginModel?.saveModel()
                            self.mobileNumberField.text = ""
                            self.clearButtonShowHide(count: 0)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    } else if otpModel.code == 300 {
                         self.isTimerActivated = false
                        DispatchQueue.main.async {
                            self.showAlert(otpModel.msg ?? "", image: nil, simChanges: false)
                        }
                        
//                        self.delegate?.didEncounteredErrorWhilePayment(errMsg: NSError.init(domain: otpModel.msg ?? "", code: 300, userInfo: nil))
                    }
                }
            }
        }
        
    }
    
    func preLoginApiCalling(agentCode: String) {
        
        guard var mobileNumber = self.mobileNumberField.text else { return }
        
        guard var countrycode = self.country?.dialCode else { return }
        
        if countrycode == "+95", mobileNumber.hasPrefix("0") {
            let _ = mobileNumber.remove(at: String.Index.init(encodedOffset: 0))
        }
        
        countrycode  = countrycode.replacingOccurrences(of: "+", with: "00")
        
        mobileNumber = countrycode + mobileNumber
        
        guard  mobileNumber.count > 0 else {
            self.delegate?.didEncounteredErrorWhilePayment(errMsg: NSError(domain: OKPayConstants.messagesKeys.mobiileNumberEmpty, code: 30, userInfo: nil))
            return
        }
        
        let domain = self.getURL(type: self.model?.urlType ?? .production)
        
        let finalString = domain + OKPayConstants.url.preLogin
        
        guard let url = URL.init(string: finalString) else { return }
        
        let otpRequest = PreLoginAgentOtpRequest(agentCode: mobileNumber, clientIp: getIPAddress() ?? "", requestType: "AUTH", clientOs: "0")
        let login      = PreLoginLogin(mobileNumber: mobileNumber, otp: OKPayConstants.global.uuid, simid: OKPayConstants.global.uuid, ostype: "1", msid: getMsid())
        let requestModel = PreLoginRequestModel(agentOtpRequest: otpRequest, isSendOtp: false, osVersion: "1", login: login, appId: "1029")
        do {
            let encodedData = try JSONEncoder().encode(requestModel)
            self.genericApi(url: url, data: encodedData, httpMethod: .post) { (data, success) in
                if success, let rawData = data {
                    do {
                        let model = try JSONDecoder().decode(PreLoginResponse.self, from: rawData)
                        if model.code == 200 {
                            
                            guard let data = model.data?.data(using: String.Encoding.utf8) else { return }
                            do {
                                let responseLogin = try JSONDecoder().decode(PreLoginDataResponse.self, from: data)
                                self.passwordType = responseLogin.passwordType ?? ""
                                guard let xmlString = responseLogin.estelResponse else { return }
                                let xml = SWXMLHash.parse(xmlString)
                                self.enumerate(indexer: xml)
                                DispatchQueue.main.async {
                                    PTLoader.shared.hide()
                                    if let str = preLoginDict["resultcode"] as? String, str == "0" {
                                        print("****************** calling after SMS API")
                                     self.afterSMSApi()
                                        guard let safeModel = self.model else { return }
                                        let touple = (safeModel.countryCode ?? "", safeModel.destination ?? "")
                                        self.msgObj = MessageManager(touple, withURL: safeModel.urlType ?? .production)
                                        self.msgObj?.publicMessage(self)
                                    } else if let str = preLoginDict["resultcode"] as? String, str == "9" {
                                        let text = ((UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en") ? "This Number is not registered with OK$" : "ဖုန္းနံပါတ္သည္ OK$ တြင္အေကာင့္ ဖြင့္လွစ္ထားျခင္းမရွိပါ။"
                                        self.clearButtonShowHide(count: 0)
                                        self.verifyButton.backgroundColor = UIColor.lightGray
                                        self.verifyButton.isUserInteractionEnabled = false
                                        self.showAlert(text, image: nil, simChanges: false)
                                    } else {
                                        self.showAlert(preLoginDict["resultdescription"] as? String ?? "", image: nil, simChanges: false)
                                    }
                                }
                                
                            } catch {
                                self.log(error)
                            }
                        } else {
                            
                        }
                    } catch {
                        self.log(error)
                    }
                }
            }
        } catch {
            self.log(error)
        }
        
    }
    
    
}
