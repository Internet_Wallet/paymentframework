//
//  PatternViewController.swift
//  OKPaymentFramework
//
//  Created by Ashish on 12/3/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit

class PatternViewController: UIViewController {
    
    @IBOutlet weak var patternLockView: GesturePatternLock!
     @IBOutlet weak var headerLabel: UILabel!
    
    weak var delegate : OKPaymentDelegates?
    
    weak var patternDelegate : PatternDelegate?
    
    var passwordString : String? = ""
    
    var agentCode : String? = ""
    
    let alertViewObj = AlertView()
    
    var blueColor = UIColor.init(red: 3.0 / 255.0, green: 33.0 / 255.0, blue: 170.0 / 255.0, alpha: 1.0)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        
        if UserDefaults.standard.value(forKey: "currentLanguage") == nil {
            currentLanguage = "my"
            UserDefaults.standard.set("my", forKey: "currentLanguage")
            updateUIMy()
        } else if (UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en" {
            updateUI()
        } else {
            updateUIMy()
        }
    }
    
    private func updateUI() {
        self.headerLabel.text = "Welcome to OK$ Payment Gateway"
    }
    
    private func updateUIMy() {
        self.headerLabel.text = "OK$ ေငြေပးေခ် ဝန္ေဆာင္မႈမွ ႀကိဳဆုိပါသည္။"
    }
    
    
    
    private func setup() {
        
        // Set number of sensors
        patternLockView.lockSize = (3, 3)
        
        // Sensor grid customisations
        patternLockView.edgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // Sensor point customisation (normal)
        patternLockView.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 3,
            color: blueColor,
            forState: .normal
        )
        patternLockView.setSensorAppearance(
            type: .outer,
            color: blueColor,
            forState: .normal
        )
        
        
        // Sensor point customisation (selected)
        patternLockView.setSensorAppearance(
            type: .inner,
            radius: 6,
            width: 6,
            color: blueColor,
            forState: .normal
        )
        patternLockView.setSensorAppearance(
            type: .outer,
            radius: 0,
            width: 1,
            color: .green,
            forState: .selected
        )
        
        // Sensor point customisation (wrong password)
        patternLockView.setSensorAppearance(
            type: .inner,
            radius: 03,
            width: 15,
            color: .red,
            forState: .error
        )
        patternLockView.setSensorAppearance(
            type: .outer,
            radius: 0,
            width: 1,
            color: .red,
            forState: .error
        )
        
        // Line connecting sensor points (normal/selected)
        [GesturePatternLock.GestureLockState.normal, GesturePatternLock.GestureLockState.selected].forEach { (state) in
            patternLockView.setLineAppearance(
                width: 5.5,
                color: .clear, // UIColor.yellow.withAlphaComponent(0.5)
                forState: state
            )
        }
        
        // Line connection sensor points (wrong password)
        patternLockView.setLineAppearance(
            width: 5.5,
            color: UIColor.red.withAlphaComponent(0.5),
            forState: .error
        )
        
        patternLockView.addTarget(
            self,
            action: #selector(gestureComplete),
            for: .gestureComplete
        )
        
    }
    
    fileprivate func showAlert(_ str: String, image: UIImage?) {
        // Please enter your valid mobile numbe
        
        self.alertViewObj.wrapAlert(title: "", body: str, img: nil)
        self.alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        self.alertViewObj.showAlert(controller: self)
        
        
        
//        let alertView  = UIAlertController(title: "", message: str, preferredStyle: .alert)
//        alertView.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
//
//        }))
//        self.present(alertView, animated: true, completion: nil)
    }
    
    @objc func gestureComplete(gestureLock: GesturePatternLock) {
        
        if gestureLock.lockSequence.count <= 3 {
            gestureLock.gestureLockState = .normal
            self.showAlert(OKPayConstants.messagesKeys.patternError, image: nil)
            //delegate?.didEncounteredErrorWhilePayment(errMsg: NSError.init(domain: OKPayConstants.messagesKeys.patternError, code: OKPayConstants.errorCode.patternCountError, userInfo: nil))
            return
        }
        
        passwordString = ""
        for element in gestureLock.lockSequence {
            passwordString?.append(element.stringValue)
        }
        
        self.passwordProcessingMethods(pass: passwordString ?? "")
        
        gestureLock.gestureLockState = .normal
    }
    
    func passwordProcessingMethods(pass: String) {
        let object = PatternValidationClass()
        let passEncrypt = object.patternEncryption(passwordString, withnumber: agentCode)
        self.patternDelegate?.patterProcessed(passEncrypt ?? "")
    }
}
