 //
//  PasswordViewController.swift
//  OKPaymentFramework
//
//  Created by Ashish on 12/3/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit

class PasswordViewController: UIViewController, UITextFieldDelegate {

    weak var delegate : OKPaymentDelegates?
     @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var mobileNumber : mobileViewField!
    @IBOutlet weak var password     : FloatLabelTextField!
    
    @IBOutlet weak var passwordRightView: UIImageView! {
        didSet {
            self.passwordRightView.image = passwordRightView.image?.withRenderingMode(.alwaysTemplate)
            self.passwordRightView.tintColor = UIColor.init(red: 3.0 / 255.0, green: 33.0 / 255.0, blue: 170.0 / 255.0, alpha: 1.0)
        }
    }
    
    var agentCode : String?
    var country: Country?
    
    @IBOutlet weak var passwordLeftView: UIImageView! {
        didSet {
            self.passwordLeftView.image = passwordLeftView.image?.withRenderingMode(.alwaysTemplate)
            self.passwordLeftView.tintColor = UIColor.init(red: 3.0 / 255.0, green: 33.0 / 255.0, blue: 170.0 / 255.0, alpha: 1.0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.password.isSecureTextEntry = true
        password.attributedPlaceholder = NSAttributedString(string: "Enter OK$ Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 3.0 / 255.0, green: 33.0 / 255.0, blue: 170.0 / 255.0, alpha: 1.0)])
        password.autocorrectionType = .no
        
        let countryView = PaytoViews.updateView()
        guard let countryDetails = country else {
            return
        }
        let countryCode = String(format: "(%@)", countryDetails.dialCode)
        countryView?.wrapCountryViewData(img: countryDetails.name.lowercased(), str: countryCode)
        countryView?.delegate = nil
        countryView?.countryLabel.textColor = UIColor.init(red: 3.0 / 255.0, green: 33.0 / 255.0, blue: 170.0 / 255.0, alpha: 1.0)
        self.mobileNumber.leftViewMode = .always
        self.mobileNumber.leftView     = countryView
        print("Changes after.")
        
        if UserDefaults.standard.value(forKey: "currentLanguage") == nil {
            currentLanguage = "my"
            UserDefaults.standard.set("my", forKey: "currentLanguage")
            updateUIMy()
        } else if (UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en" {
            updateUI()
        } else {
            updateUIMy()
        }
    }
    
    private func updateUI() {
        self.headerLabel.text = "Welcome to OK$ Payment Gateway"
        self.mobileNumber.text = "Show Account Number"
        self.password.placeholder = "Enter OK$ Password"
    }
    
    private func updateUIMy() {
        self.headerLabel.text = "OK$ ေငြေပးေခ် ဝန္ေဆာင္မႈမွ ႀကိဳဆုိပါသည္။"
        self.mobileNumber.text = "အေကာင့္နံပါတ္ ၾကည့္ရန္"
        self.password.placeholder = "OK$ လွ်ဳိ႕ဝွက္နံပါတ္ ရိုက္ထည့္ပါ"
    }
    
    //MARK:- Textfield Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == mobileNumber {
            return false
        } else {
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.password {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if text.count > 0 {
                if (UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en" {
                    textField.placeholder = "Password"
                } else {
                    textField.placeholder = "လွ်ဳိ႕၀ွက္နံပါတ္"
                }
            } else {
                if (UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en" {
                    password.attributedPlaceholder = NSAttributedString(string: "Enter OK$ Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 3.0 / 255.0, green: 33.0 / 255.0, blue: 170.0 / 255.0, alpha: 1.0)])
                } else {
                    password.attributedPlaceholder = NSAttributedString(string: "OK$ လွ်ဳိ႕ဝွက္နံပါတ္ ရိုက္ထည့္ပါ", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 3.0 / 255.0, green: 33.0 / 255.0, blue: 170.0 / 255.0, alpha: 1.0)])
                }
                
            }
            return true
        }
        return false
    }
    
    func returnPassword() -> (String, Bool) {
        return ((self.password.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines), false)
    }

    
    //MARK:- Show Account Number button action
    @IBAction func showAccountButtonAction(_ sender: UIButton) {
        if (UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en" {
           self.mobileNumber.text = "Show Account Number"
        } else {
           self.mobileNumber.text = "အေကာင့္နံပါတ္ ၾကည့္ရန္"
        }
        
    }
    @IBAction func showAccountNumberTouchDownEvent(_ sender: UIButton) {
       // self.mobileNumber.text = agentCode
        
        if let hasvalue = agentCode{
           if hasvalue.hasPrefix("0095") {
                self.mobileNumber.text = "0\(hasvalue.dropFirst(4))"
            } else {
                self.mobileNumber.text = "\(hasvalue.dropFirst(4))"
            }
        }
            
        
    }
    
    
    //MARK:- Password button action
    @IBAction func passwordBtnAction(_ sender: UIButton) {
        self.password.isSecureTextEntry = !self.password.isSecureTextEntry
        self.passwordRightView.isHighlighted = !self.password.isSecureTextEntry
        self.passwordRightView.tintColor = UIColor.init(red: 3.0 / 255.0, green: 33.0 / 255.0, blue: 170.0 / 255.0, alpha: 1.0)
    }
    
}
 
 class mobileViewField: UITextField {
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        print("changes before")
        return CGRect(x: 0, y: 3, width: 100, height: 40)
    }
 }
