//
//  LoginViewController.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/26/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit

let appDel = UIApplication.shared.delegate as! AppDelegate

 class LoginViewController: UIViewController, OKApiHandler, HelperFunctions, PatternDelegate {

    weak var delegate : OKPaymentDelegates?
    
    var model : OKPayModel?
    
    var loginModel : LoginAccess?
    
    @IBOutlet weak var passwordContainerView: UIView!
    @IBOutlet weak var patternContainerView: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    var alertViewObj = AlertView()
    
    
    @IBOutlet weak var mobChangeBtn: UIButton! {
        didSet {
            self.mobChangeBtn.setTitle("Switch Account", for: .normal)
        }
    }
    
    @IBOutlet weak var switchLoginTypeBtn: UIButton!

    var estelResponse : Dictionary<String, Any>?
    
    var agentCode : String? = ""
    
    var password : (pass: String, type: Bool) = ("",false)
    
    var passwordVC : PasswordViewController?
    
    var patternVC : PatternViewController?
    
    var isPattern: String?
//    {
//        didSet {
//            if isPattern {
//
//            } else {
//                self.patternContainerView.isHidden  = true
//                self.passwordContainerView.isHidden = false
//                self.loginBtn.isHidden = false
//            }
//        }
//    }
    
    var country : Country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
    
    override  func viewDidLoad() {
        super.viewDidLoad()

        OKPayConstants.global.defaultSet.set(true, forKey: OKPayConstants.helper.verifyKey)
      //  switchLoginTypeBtn.isHidden = true
      //  self.isPattern = false
        
        self.hideKeyboardWhenTappedAround()
        
        if OKPayConstants.global.defaultSet.bool(forKey: OKPayConstants.helper.verifyKey) {
            self.agentCode = LoginAccess.retrieveModel().number
            self.isPattern = LoginAccess.retrieveModel().isPattern

            self.country = Country.init(name: LoginAccess.retrieveModel().name, code: LoginAccess.retrieveModel().name.lowercased(), dialCode: LoginAccess.retrieveModel().code)
            
            self.loginModel = LoginAccess.init(mobile: LoginAccess.retrieveModel().number, country: country, isPattern: LoginAccess.retrieveModel().isPattern)
            self.loginModel?.saveModel()
        }
        
        if UserDefaults.standard.value(forKey: "currentLanguage") == nil {
            currentLanguage = "my"
            UserDefaults.standard.set("my", forKey: "currentLanguage")
            updateUIMy()
        } else if (UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en" {
            updateUI()
        } else {
            updateUIMy()
        }
        
        if let hasValue = isPattern{
            switchLoginTypeBtn.isHidden = true
            if hasValue == "1" {
                self.passwordContainerView.isHidden = true
                self.patternContainerView.isHidden  = false
                self.loginBtn.isHidden = true
            }else if hasValue == "0"{
                self.patternContainerView.isHidden  = true
                self.passwordContainerView.isHidden = false
                self.loginBtn.isHidden = false
            }else{
                switchLoginTypeBtn.isHidden = false
                self.patternContainerView.isHidden  = true
                self.passwordContainerView.isHidden = false
                self.loginBtn.isHidden = false
            }
        }else{
            switchLoginTypeBtn.isHidden = false
            self.patternContainerView.isHidden  = true
            self.passwordContainerView.isHidden = false
            self.loginBtn.isHidden = false
        }
    }
    
    private func updateUI() {
        self.mobChangeBtn.setTitle("Switch Account", for: .normal)
        self.switchLoginTypeBtn.setTitle("Switch Login Type", for: .normal)
       // self.switchLoginTypeBtn.setTitle("Cancel", for: .normal)
        self.loginBtn.setTitle("Login", for: .normal)
    }
    
    private func updateUIMy() {
        self.mobChangeBtn.setTitle("ဖုန္းနံပါတ္ေျပာင္း၍ အေကာင့္ထဲ၀င္ရန္", for: .normal)
        self.switchLoginTypeBtn.setTitle("အေကာင့္ဝင္သည့္ အမ်ဳိးအစား ေျပာင္းရန္", for: .normal)
        self.cancelBtn.setTitle("မလုပ္ပါ", for: .normal)
        self.loginBtn.setTitle("အေကာင့္ထဲ ဝင္ရန္", for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false

    }
    
    //MARK:- Back Button
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        if OKPayConstants.global.defaultSet.bool(forKey: OKPayConstants.helper.verifyKey) {
            self.navigationController?.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }

        
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissKeyboards))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboards() {
        view.endEditing(true)
    }
    
    fileprivate func showAlert(_ str: String, image: UIImage?) {
        // Please enter your valid mobile number
        DispatchQueue.main.async {
            
            self.alertViewObj.wrapAlert(title: "", body: str, img: nil)
            self.alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                if str == "Your device/sim changed. Please do Re-Verify" || str == "Payment Failed" {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            self.alertViewObj.showAlert(controller: self)
            
//
//            let alertView  = UIAlertController(title: "", message: str, preferredStyle: .alert)
//            alertView.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
//                if str == "Your device/sim changed. Please do Re-Verify" || str == "Payment Failed" {
//                    self.navigationController?.popViewController(animated: true)
//                }
//            }))
//            self.present(alertView, animated: true, completion: nil)
        }
    }

    //MARK:- Pattern & Password Button Action
    @IBAction func patternSelectionAction(_ sender: UIButton) {
        if let hasValue = self.isPattern{
            if hasValue == "1" {
                self.isPattern = "1"
            } else if hasValue == "0"  {
                self.isPattern = "0"
            }
        }
    }
    
    //MARK:- LoginButton Action
    
    @IBAction func loginAction(_ sender: Any) {
        self.password = passwordVC?.returnPassword() ?? ("", false)
        if self.password.pass.count > 0 {
            self.loginCall(self.password.pass, type: "0")
        } else {
               
            self.showAlert("Please enter OK$ Password", image: nil)
        }
    }
    
    private func loginCall(_ password: String, type: String) {
        let domain = getURL(type: self.model?.urlType ?? .production)
        let finalString = domain + OKPayConstants.url.login
        
        print("%@ : url",finalString)
        print("%@ : UDID",OKPayConstants.global.uuid)
        
        let model = LoginApiRequest.init(androidVersion: "", appId: setUniqueIDLogin(), authCode: "", authCodeStatus: "", bssid: "", bluetoothAddress: "", bluetoothName: "", cellId: "", connectedNetworkType: "", dateOfBirth: "", deviceSoftwareVersion: UIDevice.current.systemVersion, fatherName: "", hiddenSsid: "1", ipAddress: "", iosOtp: OKPayConstants.global.uuid, latitude: "0.0", linkSpeed: "1", longitude: "0.0", macAddress: "", mobileNumber: agentCode, msid: getMsid(), name: "", networkCountryIso: "", networkId: "1", networkOperator: "", networkOperatorName: "", networkSignal: "1", networkType: "", osType: "1", password: password, passwordType: type, profilePic: "", reRegisterOtp: OKPayConstants.global.uuid, simCountryIso: "", simOperator: "", simOperatorName: "", ssid: "", simId: OKPayConstants.global.uuid, voiceMailNo: "", isNetworkRoaming: "1")
        do {
            let encodedData = try JSONEncoder().encode(model)
            
            genericApi(url: URL(string: finalString)!, data: encodedData, httpMethod: .post) { (response, success) in
                do {
                    if let data = response {
                        let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        if let dic = dict as? Dictionary<String,AnyObject> {
                            if let code = dic.safeValueForKey("Code") as? Int, code == 200 {
                                if let login = dic.safeValueForKey("Data") as? String, login.count > 0 {
                                    if let dicRep = self.convertToDictionary(text: login) {
                                        if let log = dicRep.safeValueForKey("AgentDetails") as? Array<Any> {
                                            if let logi = log.first as? Dictionary<String,Any> {
                                                if (logi.safeValueForKey("ResultDescription") as? String)?.lowercased() == "Transaction Successful".lowercased() {
                                                    let token = logi.safeValueForKey("securetoken") as? String ?? ""
                                                    self.genericPaymentAPI(token: token, passwordString: password)
                                                } else {
                                                    let _ = NSError.init(domain: logi.safeValueForKey("ResultDescription") as? String ?? "Login Failed", code: dic.safeValueForKey("Code") as? Int ?? 0, userInfo: nil)
                                                    //self.delegate?.didEncounteredErrorWhilePayment(errMsg: error)
                                                    self.showAlert(logi.safeValueForKey("ResultDescription") as? String ?? "Login Failed", image: nil)
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                if let code = dic["Code"] as? NSNumber, code == 300 {
                                    let _ = NSError.init(domain: "Your device/sim changed. Please do Re-Verify", code: dic.safeValueForKey("Code") as? Int ?? 0, userInfo: nil)
                                    let text = ((UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en") ? "Please reverify your sim in okdollar and try again" : "သင့္စက္ေျပာင္းထားေသာေၾကာင့္ အေကာင့္ထဲျပန္ဖြင့္ပါ"
                                    self.showAlert(text, image: nil)
                                } else if let code = dic["Code"] as? NSNumber, code == 306 {
                                    let text = ((UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en") ? "Login Not Match with OK$" : "OK$ အေကာင့္ ကိုက္ညီမႈ မရွိပါ"
                                    let _ = NSError.init(domain: "Login Not Match with OK$", code: dic.safeValueForKey("Code") as? Int ?? 0, userInfo: nil)
                                    self.showAlert(text, image: nil)
                                    //self.delegate?.didEncounteredErrorWhilePayment(errMsg: error)
                                } else if let code = dic["Code"] as? NSNumber, code == 307 {
                                    let text = ((UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en") ? "Safety Cashier can not able login in OK$" : "OK$ တြင္ Safety Cashier အေကာင့္ကို ဝင္မရပါ"
                                    let _ = NSError.init(domain: "Safety Cashier can not able login in OK$", code: dic.safeValueForKey("Code") as? Int ?? 0, userInfo: nil)
                                    self.showAlert(text, image: nil)
                                    //self.delegate?.didEncounteredErrorWhilePayment(errMsg: error)
                                } else {
                                    let _ = NSError.init(domain: dic["Msg"] as! String, code: dic.safeValueForKey("Code") as? Int ?? 0, userInfo: nil)
                                   
                                    if let message = dic["Msg"] as? String, message == "Invalid pin".lowercased(), type != "0" {
                                        let text = ((UserDefaults.standard.value(forKey: "currentLanguage") as? String ?? "") == "en") ? "Invalid PIN" : "သင္႐ိုက္ထည့္ေသာ လွ်ိဳ႕၀ွက္နံပါတ္ မမွန္ပါ"
                                        self.showAlert(text, image: nil)
                                    } else {
                                        self.showAlert(dic["Msg"] as! String, image: nil)
                                    }
                                    //self.delegate?.didEncounteredErrorWhilePayment(errMsg: error)
                                }
                                
                            }
                        }
                    }
                } catch {
                    self.log(error)
                }
            }
        } catch {
            
        }

    }
    
    private func genericPaymentAPI(token: String, passwordString: String) {
        guard let model = self.model else {
            let error = NSError.init(domain: "Model Init NSError", code: 500, userInfo: nil)
            self.delegate?.didEncounteredErrorWhilePayment(errMsg: error)
            return
        }
        let lreference = GenericPaymentLExternalReference.init(direction: true, endStation: "", startStation: "", vendorId: "")
        let geoLoc     = GenericPaymentLGeoLocation.init(cellId: "", latitude: model.geoTuple?.lat ?? "0.0", longitude: model.geoTuple?.long ?? "0.0")
        let lProximity = GenericPaymentLProximity.init(blueToothUsers: [], selectionMode: true, wifiUsers: [])
        
        let identifier = "Paymentgateway \(Bundle.main.bundleIdentifier ?? "")"
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE MM-dd-yyyy HH:mm a"
        let dateString = formatter.string(from: date)
        
        let lTransaction = GenericPaymentLTransactions.init(amount: ((model.amount ?? 0.0) as NSNumber).stringValue, balance: "0000", bonusPoint: 0, comments: identifier, destination: model.destination ?? "", kickBack: "0", localTransactionType: "Payto", mobileNumber: loginModel?.mobileNumber ?? "", mode: true, password: passwordString, resultCode: 0, resultDescription: "", secureToken: token, transactionId: "", transactionTime: dateString, transactionType: "PAYTO")
        let genericPayModel = GenericPaymentRequestModel.init(lExternalReference: lreference, lGeoLocation: geoLoc, lProximity: lProximity, lTransactions: lTransaction)
        
        let domain = getURL(type: model.urlType ?? .production)
        let finalString = domain + OKPayConstants.url.genericPay
        
        do {
            let encodedData = try JSONEncoder().encode(genericPayModel)
            genericApi(url: URL.init(string: finalString)!, data: encodedData, httpMethod: .post) { (data, success) in
                if success {
                    print("Sucessfully calls the payment done")
                    self.delegate?.paymentSuccessfull(raw: data)
                } else {
                    print("payment Failure calls")
                    self.showAlert("Payment Failed", image: nil)
                    //self.delegate?.didEncounteredErrorWhilePayment(errMsg: NSError.init(domain: "Payment Failed", code: 300, userInfo: nil))
                }
            }

        } catch {
            log(error)
        }
    }
    
    
    //MARK:- Reverify Mobile Number
    @IBAction func reVerifyAction(_ sender: UIButton) {
        OKPayConstants.global.defaultSet.set(false, forKey: OKPayConstants.helper.verifyKey)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Navigation Controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if OKPayConstants.global.defaultSet.bool(forKey: OKPayConstants.helper.verifyKey) {
            self.agentCode = LoginAccess.retrieveModel().number
            self.isPattern = LoginAccess.retrieveModel().isPattern
            self.country = Country.init(name: LoginAccess.retrieveModel().name, code: LoginAccess.retrieveModel().name.lowercased(), dialCode: LoginAccess.retrieveModel().code)
            self.loginModel = LoginAccess.init(mobile: LoginAccess.retrieveModel().number, country: country, isPattern: LoginAccess.retrieveModel().isPattern)
            self.loginModel?.saveModel()
        }

        
        if let password = segue.destination as? PasswordViewController {
            password.delegate  = self.delegate
            password.agentCode = self.agentCode
            password.country   = self.country
            passwordVC = password
        } else if let pattern = segue.destination as? PatternViewController {
            pattern.delegate = self.delegate
            pattern.patternDelegate = self
            pattern.agentCode = self.agentCode
            patternVC = pattern
        }
    }
    
    //MARK:- Pattern Processed
    func patterProcessed(_ hash: String) {
        self.loginCall(hash, type: "1")
    }

}



class Localization: UIViewController {
    var localBundle: Bundle?
    
    func setInitialCurrentLang() {
        if UserDefaults.standard.value(forKey: "currentLanguage") == nil {
            currentLanguage = "my"
            UserDefaults.standard.set("my", forKey: "currentLanguage")
            let bundle = Bundle(for: PreLoginViewControlelr.self)
            if let path = bundle.path(forResource: "my", ofType: "strings") {
                localBundle = Bundle(path: path)
            }
        } else {
            let bundle = Bundle(for: PreLoginViewControlelr.self)
            if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), let path =  bundle.path(forResource: lang, ofType: "strings") {
                currentLanguage = lang
                localBundle = Bundle(path: path)
            }
        }
    }
    
    func setSeletedlocaLizationLanguage(language : String) {
        currentLanguage = language
        let bundle = Bundle(for: PreLoginViewControlelr.self)
        if let path = bundle.path(forResource: currentLanguage, ofType: "strings") {
            localBundle = Bundle(path: path)
        }
    }
    
    func getSelectedLanguage() -> String {
        return currentLanguage
    }
    
    func getlocaLizationLanguage(key : String) -> String {
        localBundle = Bundle(for: PreLoginViewControlelr.self)
        if let bundl = localBundle {
//            print("Localization called with bundle \(bundl)")
//            print("Test \(NSLocalizedString(key, tableName: "LocalizationFramework", bundle: bundl, value: "", comment: ""))")
            return NSLocalizedString(key, tableName: "LocalizationFramework", bundle: bundl, value: "", comment: "")
        } else {
            return key
        }
    }
    
}



extension String {
   var localized: String {
        return appDel.getlocaLizationLanguage(key: self)
    }
}

    

